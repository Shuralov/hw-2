const {src, dest, series, parallel, watch} = require('gulp');
const browserSync = require('browser-sync').create(),
sass = require('gulp-sass'),
postcss = require('gulp-postcss'),
uncss = require('postcss-uncss'),
del = require('del'),
minify = require('gulp-minify'),
concat = require('gulp-concat'),
cssnano = require('gulp-cssnano'),
rename = require('gulp-rename'),
autoprefixer = require('gulp-autoprefixer'),
cleanCSS = require('gulp-clean-css'),
imagemin = require('gulp-imagemin'),
useref = require('gulp-useref'),
rep = require('gulp-replace-image-src');

function emptyDist() {
    return del('./dist/**');
}


function copyHtml() {
    return src('./src/index.html')
        .pipe(useref())
        .pipe(rep({
            prependSrc : './dist/assets/img/',
            keepOrigin : false
        }))

        .pipe(dest('./'))
        .pipe(browserSync.reload({ stream: true }));
}

function buildJs() {
    return src('./src/js/index.js')
        .pipe(minify())
        .pipe(concat('all.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('./dist/js'))
}

function buildCss() {
    return src('./src/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./index.html']})
        ]))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(cssnano())
        .pipe(concat('all.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.reload({ stream: true }));
}

function buildSrcCss() {
    return src('./src/scss/**/*.scss')
        .pipe(sass())
        .pipe(cssnano())
        .pipe(concat("./dist/css/bundle.css"))
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('./dist/css'))
        .pipe(dest('./src/css'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'));
}

function serve() {
    browserSync.init({
        server: './src'
    });
    watch(['./src/index.html'], copyHtml);
    watch(['./src/scss/*.scss'], buildCss);
    watch(['./src/js/*.js'], buildJs);
}

function imagesMinifier() {
    return src('src/assets/img/**')
        .pipe(imagemin())
        .pipe(dest('./dist/assets/img/'))
}

exports.html = copyHtml;
exports.scss = buildCss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = buildJs;
exports.css = buildSrcCss;
exports.serve = serve;
exports.imgmin = imagesMinifier;

exports.build = series(
    emptyDist,
    parallel(
        series(copyHtml, buildCss, buildJs, imagesMinifier)
    )
);

exports.dev = serve;

exports.default = () => src(
    "style.scss"
).pipe(sass()).pipe(dest("./"));










