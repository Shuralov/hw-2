const menu = document.querySelector('.menu');
const burger = document.querySelector('.menu--burger');
const navElems = document.querySelector('.nav--elems');
const span = document.querySelector('.menu--burger-sign');
let expanded = false;

menu.addEventListener('click', expand);

function expand () {
    if (expanded === true) {
        navElems.classList.remove('active');
        navElems.classList.add('nav--elems-hide');
        burger.classList.remove('active-burger');
        span.classList.remove('hide');
        expanded = false;
    } else {
        navElems.classList.remove('nav--elems-hide');
        navElems.classList.add('active');
        burger.classList.add('active-burger');
        span.classList.add('hide');
        expanded = true;
    }
}